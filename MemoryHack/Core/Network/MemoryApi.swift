import Alamofire
import Combine

enum MemoryAPI {
    static let agent = Agent()
    static let base = URL(string: "https://gentle-dawn-59875.herokuapp.com")!
}

extension MemoryAPI {
    static func fetch() -> AnyPublisher<[ArchiveReponce], Error> {
        agent.run(URLRequest(url: base.appendingPathComponent("getPhoto")))
            .map(\.value)
            .eraseToAnyPublisher()
    }
    
    static func uploadImage(from url: URL, with parameters: [String: String]) {
        AF.upload(multipartFormData: { multipartFormData in
            for (key,value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            multipartFormData.append(url, withName: "filename")
        }, to: "https://gentle-dawn-59875.herokuapp.com/uploadPhoto").responseJSON {
            print($0)
        }
    }
    
    static func processPhoto(from url: URL) {
        let parameters = ["sex": "f"]
        AF.upload(multipartFormData: { multipartFormData in
            for (key,value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            multipartFormData.append(url, withName: "filename")
        }, to: "https://gentle-dawn-59875.herokuapp.com/processPhoto").responseJSON {
            print($0)
        }
    }
}
