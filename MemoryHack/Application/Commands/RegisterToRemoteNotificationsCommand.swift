import UIKit.UIApplication

struct RegisterToRemoteNotificationsCommand: Command {
    let application: UIApplication
    
    func execute() {
        application.registerForRemoteNotifications()
    }
}
