import UIKit

struct InitializeAppearanceCommand: Command {
    func execute() {
        if ProcessInfo().arguments.contains("SKIP_ANIMATIONS") {
            UIView.setAnimationsEnabled(false)
        }
    }
}
