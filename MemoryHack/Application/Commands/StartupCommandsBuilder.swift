import UIKit

final class StartupCommandsBuilder {
    private var application: UIApplication!
    
    func setApplication(_ application: UIApplication) -> StartupCommandsBuilder {
        self.application = application
        return self
    }
    
    func build() -> [Command] {
        [
            InitializeThirdPartiesCommand(),
            InitializeAppearanceCommand(),
            RegisterToRemoteNotificationsCommand(application: application)
        ]
    }
}
