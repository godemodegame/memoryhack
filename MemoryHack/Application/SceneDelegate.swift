import UIKit

@available(iOS 13.0, *)
final class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    private var rootController: UINavigationController {
        window?.rootViewController = UINavigationController(rootViewController: UIViewController())
        window?.makeKeyAndVisible()
        return window?.rootViewController as! UINavigationController // swiftlint:disable:this force_cast
    }
    
    private lazy var applicationCoordinator: Coordinator = self.produceApplicationCoordinator()
    
    private func produceApplicationCoordinator() -> Coordinator {
        ApplicationCoordinator(router: RouterImp(rootController: rootController),
                               coordinatorFactory: CoordinatorFactoryImp(),
                               flowFactory: FlowFactoryImp())
    }
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        
        applicationCoordinator.start()
    }
    
    func sceneDidDisconnect(_ scene: UIScene) { }
    func sceneDidBecomeActive(_ scene: UIScene) { }
    func sceneWillResignActive(_ scene: UIScene) { }
    func sceneWillEnterForeground(_ scene: UIScene) { }
    func sceneDidEnterBackground(_ scene: UIScene) { }
}
