import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    private var rootController: UINavigationController {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: UIViewController())
        window?.makeKeyAndVisible()
        return window?.rootViewController as! UINavigationController // swiftlint:disable:this force_cast
    }
    
    private lazy var applicationCoordinator: Coordinator = self.produceApplicationCoordinator()
    
    private func produceApplicationCoordinator() -> Coordinator {
        ApplicationCoordinator(router: RouterImp(rootController: rootController),
                               coordinatorFactory: CoordinatorFactoryImp(),
                               flowFactory: FlowFactoryImp())
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if #available(iOS 13, *) { } else {
            window = UIWindow(frame: UIScreen.main.bounds)
            applicationCoordinator.start()
        }
        
        application.applicationIconBadgeNumber = 0
        
        UNUserNotificationCenter.current().delegate = self
        
        StartupCommandsBuilder()
            .setApplication(application)
            .build()
            .forEach { $0.execute() }
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) { }
}

