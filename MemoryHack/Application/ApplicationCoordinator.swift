final class ApplicationCoordinator: BaseCoordinator {
    private let flowFactory: FlowFactoryImp
    private let coordinatorFactory: CoordinatorFactory
    private let router: Router
    
    init(router: Router, coordinatorFactory: CoordinatorFactory,
         flowFactory: FlowFactoryImp) {
        
        self.router = router
        self.coordinatorFactory = coordinatorFactory
        self.flowFactory = flowFactory
    }
    
    override func start() {
        UserDefaultsConfig.isFirstLaunch = false
        if UserDefaultsConfig.isFirstLaunch {
            runOnboarding()
        } else {
            runMainFlow()
        }
    }
    
    private func runOnboarding() {
        let coordinator = coordinatorFactory.produceOnboardingCoordinator(router: router, flowFactory: flowFactory)
        
        coordinator.finishFlow = { [weak self, weak coordinator] in
            UserDefaultsConfig.isFirstLaunch = false
            self?.removeDependency(coordinator)
            self?.runMainFlow()
        }
        
        addDependency(coordinator)
        coordinator.start()
    }
    
    private func runMainFlow() {
        let coordinator = coordinatorFactory.produceMainCoordinator(router: router, flowFactory: flowFactory)
        
        coordinator.finishFlow = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
            self?.runMainFlow()
        }
        
        addDependency(coordinator)
        coordinator.start()
    }
}
