import UIKit.UIViewController

final class NotificationsRequestAssembly: Assembly {
    
    static func assembleModule() -> UIViewController {
        let view = NotificationsRequestController()
        let presenter = NotificationsRequestPresenter()
        let interactor = NotificationsRequestInteractor()
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.coordinator = view
        
        return view
    }
}
