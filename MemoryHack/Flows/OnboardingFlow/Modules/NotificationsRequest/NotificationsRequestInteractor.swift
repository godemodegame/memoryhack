import UIKit

protocol NotificationsRequestInteractorInput: AnyObject {
    func requestNotification(completion: @escaping () -> Void)
}

final class NotificationsRequestInteractor: NotificationsRequestInteractorInput {
    func requestNotification(completion: @escaping () -> Void) {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { _, _ in
            completion()
        }
    }
}
