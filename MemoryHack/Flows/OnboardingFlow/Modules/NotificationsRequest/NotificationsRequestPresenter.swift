import Foundation // swiftlint:disable:this foundation_using

protocol NotificationsRequestViewOutput: ViewOutput {
    func didTapContinue()
    func didTapLater()
}

final class NotificationsRequestPresenter: NotificationsRequestViewOutput {
    weak var coordinator: OnboardingOutput?
    var interactor: NotificationsRequestInteractorInput?
    
    func didTapContinue() {
        interactor?.requestNotification { [weak self] in
            DispatchQueue.main.async { self?.coordinator?.onFinish?() }
        }
    }
    
    func didTapLater() {
        coordinator?.onFinish?()
    }
}
