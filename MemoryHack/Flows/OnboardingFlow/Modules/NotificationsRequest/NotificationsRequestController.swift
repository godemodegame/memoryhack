import UIKit

protocol NotificationsRequestOutput: OnboardingOutput { }

final class NotificationsRequestController: UIViewController, NotificationsRequestOutput, ViewSpecificController {
    var onFinish: (() -> Void)?
    
    // MARK: AssociatedType
    
    typealias RootView = NotificationsRequestView
    
    // MARK: Private Properties
    
    var presenter: NotificationsRequestViewOutput?
    
    override func loadView() {
//        view = NotificationsRequestView()
    }
}
