protocol StartOnboardViewOutput: ViewOutput {
    func didTapContinue()
}

final class StartOnboardPresenter: StartOnboardViewOutput {
    weak var coordinator: OnboardingOutput?
    
    func didTapContinue() {
        coordinator?.onFinish?()
    }
}
