import UIKit.UIViewController

final class StartOnboardAssembly: Assembly {
    static func assembleModule() -> UIViewController {
        let view = StartOnboardController()
        let presenter = StartOnboardPresenter()
        
        view.presenter = presenter
        presenter.coordinator = view
        
        return view
    }
}
