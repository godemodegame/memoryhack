import UIKit

protocol StartOnboardOutput: OnboardingOutput { }

final class StartOnboardController: UIViewController, StartOnboardOutput, ViewSpecificController {
    var onFinish: (() -> Void)?
    
    // MARK: AssociatedType
    
    typealias RootView = StartOnboardView
    
    // MARK: Private Properties
    
    var presenter: StartOnboardViewOutput?
    
    override func loadView() {
        view = StartOnboardView()
    }
}
