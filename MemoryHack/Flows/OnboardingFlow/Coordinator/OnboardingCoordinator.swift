final class OnboardingCoordinator: BaseCoordinator, OnboardingCoordinatorOutput {
    
    // MARK: - OnboardingCoordinatorOutput
    
    var finishFlow: (() -> Void)?
    
    private let factory: OnboardingFlowFactory
    private let router: Router
    
    init(router: Router, factory: OnboardingFlowFactory) {
        self.factory = factory
        self.router = router
    }
    
    // MARK: - BaseCoordinator
    
    override func start() {
        showStartOnboard()
    }
    
    // MARK: - Flow's controllers
    
    private func showStartOnboard() {
        let startOnboardOutput = factory.produceStartOnboard()
        startOnboardOutput.onFinish = { [weak self] in
            self?.showNotificationsRequest()
        }
        router.setRootModule(startOnboardOutput, hideBar: true)
    }
    
    private func showNotificationsRequest() {
        let notificationRequestOutput = factory.produceNotificationsRequest()
        notificationRequestOutput.onFinish = { [weak self] in
            self?.finishFlow?()
        }
        router.push(notificationRequestOutput)
    }
}
