protocol OnboardingOutput: Presentable {
    var onFinish: (() -> Void)? { get set }
}
