protocol MainCoordinatorOutput: AnyObject {
    var finishFlow: (() -> Void)? { get set }
}
