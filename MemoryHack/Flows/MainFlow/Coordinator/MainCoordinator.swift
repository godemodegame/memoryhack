import UIKit

final class MainCoordinator: BaseCoordinator, MainCoordinatorOutput {
    
    // MARK: - MainCoordinatorOutput
    
    var finishFlow: (() -> Void)?
    
    private let factory: MainFlowFactory
    private let router: Router
    
    init(router: Router, factory: MainFlowFactory) {
        self.factory = factory
        self.router = router
    }
    
    // MARK: - BaseCoordinator
    
    override func start() {
        showMain()
    }
    
    // MARK: - Flow's controllers
    
    private func showMain() {
        let mainOutput = factory.produceMain()
        mainOutput.onAdd = { [weak self] in
            self?.showDetail()
        }
        
        mainOutput.onOpen = { [weak self] image in
            self?.showNeural(image: image)
        }
        router.setRootModule(mainOutput, hideBar: false)
    }
    
    private func showDetail() {
        let detailOutput = factory.produceDetail()
        detailOutput.onFinish = { [weak self] image in
            self?.showNeural(image: image)
        }
        router.push(detailOutput)
    }
    
    private func showNeural(image: UIImage) {
        let neuralOutput = factory.produceNeural(image: image)
        router.push(neuralOutput)
    }
}
