import UIKit.UICollectionView

final class MainDelegate: NSObject, UICollectionViewDelegate {
    
    let urls: [URL]
    
    init(urls: [URL]) {
        self.urls = urls
    }
    
    override init() {
        urls = []
        super.init()
    }
    
    var onAdd: (() -> Void)?
    var onOpen: ((UIImage) -> Void)?
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            onAdd?()
        } else {
            UIImage.load(url: urls[indexPath.item - 1]) { [weak self] image in
                self?.onOpen?(image)
            }
        }
    }
}
