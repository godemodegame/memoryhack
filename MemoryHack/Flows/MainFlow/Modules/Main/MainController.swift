import UIKit

protocol MainOutput: Presentable {
    var onAdd: (() -> Void)? { get set }
    var onOpen: ((UIImage) -> Void)? { get set }
}

final class MainController: UIViewController, MainOutput, ViewSpecificController {
    var onAdd: (() -> Void)?
    var onOpen: ((UIImage) -> Void)?
    
    // MARK: AssociatedType
    
    typealias RootView = MainView
    
    // MARK: Private Properties
    
    var presenter: MainViewOutput?
    
    var mainDataSource: MainDataSource? {
        didSet {
            view().collectionView.dataSource = mainDataSource
        }
    }
    
    var mainDelegate: MainDelegate? {
        didSet {
            mainDelegate?.onAdd = onAdd
            mainDelegate?.onOpen = onOpen
            view().collectionView.delegate = mainDelegate
        }
    }
    
    override func loadView() {
        view = MainView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        mainDelegate = MainDelegate()
        mainDataSource = MainDataSource(urlStrings: [])
        title = "Архив"
    }
}

extension MainController: MainViewInput {
    func display(data: [String]) {
        mainDataSource = MainDataSource(urlStrings: data)
        mainDelegate = MainDelegate(urls: data.map { URL(string: $0)! })
    }
}
