import UIKit

final class MainView: NLView {
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.register([MainCell.self])
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(collectionView)
    }
    
    override func layoutSubviews() {
        collectionView.frame = bounds
        let layout = UICollectionViewFlowLayout()
        let contentSide = frame.width / 2 - 50
        layout.sectionInset = UIEdgeInsets(top: 25, left: 25, bottom: 25, right: 25)
        layout.itemSize = CGSize(width: contentSide, height: contentSide)
        collectionView.collectionViewLayout = layout
    }
}
