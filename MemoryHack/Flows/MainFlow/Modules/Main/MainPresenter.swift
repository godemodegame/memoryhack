protocol MainViewOutput: ViewOutput {
    
}

final class MainPresenter: MainViewOutput, MainInteractorOutput {
    weak var view: MainViewInput?
    var interactor: MainInteractorInput?
    weak var coordinator: MainOutput?
    
    func viewDidLoad() {
        interactor?.observeImages()
    }
    
    // MARK: MainInteractorOutput
    
    func observationFailure(_ error: Error) {
        
    }
    
    func observationSuccesfull(_ data: [String]) {
        view?.display(data: data)
    }
}

protocol MainViewInput: AnyObject {
    func display(data: [String])
}
