import UIKit.UIViewController

final class MainAssembly: Assembly {
    static func assembleModule() -> UIViewController {
        let view = MainController()
        let presenter = MainPresenter()
        let interactor = MainInteractor()
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        presenter.coordinator = view
        interactor.presenter = presenter
        
        return view
    }
}
