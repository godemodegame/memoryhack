import Combine
import Kingfisher

protocol MainInteractorInput: AnyObject {
    func observeImages()
}

final class MainInteractor: MainInteractorInput {
    weak var presenter: MainInteractorOutput?
    
    var cancellable: AnyCancellable?
    
    func observeImages() {
        cancellable = MemoryAPI.fetch()
            .print()
            .sink(receiveCompletion: { [weak self] status in
                switch status {
                case .failure(let error):
                    self?.presenter?.observationFailure(error)
                case .finished:
                    break
                }
            }) { [weak self] responce in
                self?.presenter?.observationSuccesfull(responce.map { $0.photoName })
        }
    }
    
    deinit {
        cancellable?.cancel()
    }
}

protocol MainInteractorOutput: AnyObject {
    func observationFailure(_ error: Error)
    func observationSuccesfull(_ data: [String])
}
