import UIKit.UICollectionView

final class MainDataSource: NSObject, UICollectionViewDataSource {
    let urlStrings: [String]
    
    init(urlStrings: [String]) {
        self.urlStrings = urlStrings
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        urlStrings.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainCell.reuseId, for: indexPath)
        if indexPath.row == 0 {
            (cell as? MainCell)?.imageView.image = UIImage(named: "newImage")
        } else {
            (cell as? Decoratable)?.decorate(model: urlStrings[indexPath.item - 1])
        }
        return cell
    }
}
