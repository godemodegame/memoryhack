import UIKit
import Kingfisher

final class MainCell: NLCollectionViewCell, Reusable {
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        layer.cornerRadius = 20
        layer.masksToBounds = true
        addSubview(imageView)
    }
    
    override func layoutSubviews() {
        imageView.frame = bounds
    }
}

extension MainCell: Decoratable {
    func decorate(model: ViewModelProtocol) {
        guard let model = model as? String else { fatalError("wrong model for MainCell") }
        guard let url = URL(string: model) else { return }
        imageView.kf.setImage(with: url)
    }
}
