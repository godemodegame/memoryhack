import ImageDetect
import Alamofire

protocol NeuralOutput: Presentable {
    
}

final class NeuralController: UIViewController, ViewSpecificController, NeuralOutput {
    var image: UIImage!
    
    typealias RootView = NeuralView
    
    override func loadView() {
        view = NeuralView()
    }
    
    var images: [URL] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let parameters = ["sex": "f"]
        AF.upload(multipartFormData: { [unowned self] multipartFormData in
            for (key,value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            let jpegData = self.image.jpegData(compressionQuality: 1.0)
            multipartFormData.append(Data((jpegData)!), withName: "filename", fileName: UUID().uuidString + ".jpeg", mimeType: "image/jpeg")
            }, to: "https://gentle-dawn-59875.herokuapp.com/processPhoto").responseJSON { response in
                let json = try? JSONSerialization.jsonObject(with: response.data!, options: []) as? [String: Any]
                let data = json?["result"] as? [String]
                data?.forEach { value in
                    self.images.append(URL(string: value)!)
                }
                self.makeGif()
        }
    }
    
    func makeGif() {
        var image = Array<UIImage>()
        images.forEach {
            UIImage.load(url: $0) {
                image.append($0)
            }
        }
        let url = UIImage.animatedGif(from: image)!
        print(UIImage.gifImageWithURL(url.absoluteString))
        view().imageView.image = UIImage.gifImageWithURL(url.absoluteString)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
