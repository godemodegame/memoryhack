import UIKit

final class NeuralAssembly: Assembly {
    
    struct Model: TransitionModel {
        let image: UIImage
    }
    
    static func assembleModule(with model: TransitionModel) -> UIViewController {
        let view = NeuralController()
        view.image = (model as? Model)?.image
        return view
    }
}
