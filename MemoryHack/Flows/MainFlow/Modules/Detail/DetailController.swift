import UIKit
import Alamofire

protocol DetailOutput: Presentable {
    var onFinish: ((UIImage) -> Void)? { get set }
}

final class DetailController: UIViewController, DetailOutput, ViewSpecificController {
    var onFinish: ((UIImage) -> Void)?
    
    // MARK: AssociatedType
    
    typealias RootView = DetailView
    
    // MARK: Private Properties
    
    var presenter: DetailViewOutput?
    lazy var imagePicker = ImagePicker(presentationController: self, delegate: self)
    
    override func loadView() {
        view = DetailView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        view().button.addTarget(self, action: #selector(addButtonTapped(_:)), for: .touchUpInside)
    }
    
    @objc private func addButtonTapped(_ sender: UIButton) {
        imagePicker.present(from: sender)
    }
}

extension DetailController: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        guard let image = image else { return }
        let parameters = ["vkUserId": "1234", "name": "Лена", "surname": "Головач"]
        AF.upload(multipartFormData: { multipartFormData in
            for (key,value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            let jpegData = image.jpegData(compressionQuality: 1.0)
            multipartFormData.append(Data((jpegData)!), withName: "filename", fileName: UUID().uuidString, mimeType: "image/jpeg")
        }, to: "https://gentle-dawn-59875.herokuapp.com/uploadPhoto", method: .post).responseJSON {
            print($0)
            DispatchQueue.main.async { [weak self] in
                self?.onFinish?(image)
            }
        }
    }
}

extension DetailController: DetailViewInput {
    
}
