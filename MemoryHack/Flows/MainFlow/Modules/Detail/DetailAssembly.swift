import UIKit.UIViewController

final class DetailAssembly: Assembly {
    static func assembleModule() -> UIViewController {
        let view = DetailController()
        let presenter = DetailPresenter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.coordinator = view
        
        return view
    }
}
