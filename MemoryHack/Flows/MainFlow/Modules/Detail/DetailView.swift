import UIKit

final class DetailView: NLView {
    
    let cardView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 20
        view.layer.shadowColor = UIColor.white.withAlphaComponent(0.20).cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 7)
        return view
    }()
    
    let button: UIButton = {
        let button = UIButton()
        button.setTitle("add", for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .systemBackground
        addSubview(button)
    }
    
    override func layoutSubviews() {
        button.sizeToFit()
        button.center = center
    }
}
