import Photos

protocol DetailViewOutput: ViewOutput {
    func upload(image: PHAsset)
}

final class DetailPresenter: DetailViewOutput {
    weak var view: DetailViewInput?
    weak var coordinator: DetailOutput?
    
    func viewDidLoad() {
        
    }
    
    func upload(image: PHAsset) {
        
    }
    
    // MARK: DetailInteractorOutput
    
}

protocol DetailViewInput: AnyObject {
    
}
