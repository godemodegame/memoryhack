import Photos

extension PHAsset {
    func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)) {
        if mediaType == .image {
            let options = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = { adjustmeta in
                true
            }
            requestContentEditingInput(with: options, completionHandler: { contentEditingInput, info in
                completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
            })
        }
    }
}
