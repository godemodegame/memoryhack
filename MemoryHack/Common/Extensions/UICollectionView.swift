import UIKit

typealias ReusableCollectionViewCell = Reusable & NLCollectionViewCell

extension UICollectionView {
    func register(_ array: [ReusableCollectionViewCell.Type]) {
        array.forEach { (type) in
            self.register(type.self, forCellWithReuseIdentifier: type.reuseId)
        }
    }
}
