import UIKit.UINavigationController

final class CoordinatorFactoryImp: CoordinatorFactory {
    func produceOnboardingCoordinator(router: Router, flowFactory: OnboardingFlowFactory) -> Coordinator & OnboardingCoordinatorOutput {
        OnboardingCoordinator(router: router, factory: flowFactory)
    }
    
    func produceMainCoordinator(router: Router, flowFactory: MainFlowFactory) -> Coordinator & MainCoordinatorOutput {
        MainCoordinator(router: router, factory: flowFactory)
    }
}
