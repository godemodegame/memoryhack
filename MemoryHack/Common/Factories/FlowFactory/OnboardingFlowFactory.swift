protocol OnboardingFlowFactory: AnyObject {
    func produceNotificationsRequest() -> NotificationsRequestOutput
    func produceStartOnboard() -> StartOnboardOutput
}
