// swiftlint:disable force_cast
import UIKit.UIViewController

final class FlowFactoryImp: MainFlowFactory, OnboardingFlowFactory {
    func produceMain() -> MainOutput { MainAssembly.assembleModule() as! MainOutput }
    func produceDetail() -> DetailOutput { DetailAssembly.assembleModule() as! DetailOutput }
    func produceNeural(image: UIImage) -> NeuralOutput { NeuralAssembly.assemble(with: NeuralAssembly.Model(image: image)) as! NeuralOutput }
    
    func produceNotificationsRequest() -> NotificationsRequestOutput { NotificationsRequestAssembly.assembleModule() as! NotificationsRequestOutput }
    func produceStartOnboard() -> StartOnboardOutput { StartOnboardAssembly.assembleModule() as! StartOnboardOutput }
}
