import UIKit

protocol MainFlowFactory: AnyObject {
    func produceMain() -> MainOutput
    func produceDetail() -> DetailOutput
    func produceNeural(image: UIImage) -> NeuralOutput
}
