import UIKit.UIBarButtonItem

/* non-final */ class NLBarButtonItem: UIBarButtonItem {
    override init() {
        super.init()
    }
    
    @available(*, unavailable, message: "Loading this view from a nib is unsupported in favor of initializer dependency injection.")
    public required init?(coder aDecoder: NSCoder) {
        fatalError("Loading this view from a nib is unsupported in favor of initializer dependency injection.")
    }
}
