protocol Decoratable {
    func decorate(model: ViewModelProtocol)
}
